<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    //relacion polimorfismo muchos a muchos
    public function tags(){
        return $this->morphToMany('App\Tag', 'taggable');
    }
}
