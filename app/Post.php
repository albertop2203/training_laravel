<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Softdeletes;
class Post extends Model
{
    //variable para caber el directorio de las imagenes
    public $directory = "/images/";
    //propiedad para eliminacion por softdelete
    use Softdeletes;
    protected $dates = ['deleted_at'];

    //Propiedades del modelo Post
    protected $fillable = [
        'title',
        'content',
        'path'
    ];
      //función para mostrar los registros relacionas de un post en usuario
    public function user(){  
        return $this->belongsTo('App\User');
    } 
    //--------------------
    //version 2
    //-------------------
    //ralacion polimofica con photo
    public function photos(){
        return $this->morphMany('App\Photo', 'imageable');
    }
    //relacion polimorfismo muchos a muchos
    public function tags(){
        return $this->morphToMany('App\Tag', 'taggable');
    }
    //accesor mutatr para el path de las imagenes
    public function getPathAttribute($value){
        return $this->directory . $value;
    }

}
