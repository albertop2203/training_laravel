<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable; 
    protected $fillable = [
        'name', 'email', 'password',
    ]; 
    protected $hidden = [
        'password', 'remember_token',
    ]; 
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    //relación 1 a 1 
    public function post(){ 
        return $this->hasOne('App\Post');
    }

    //relación 1 a N 
    public function posts(){
        return $this->hasMany('App\Post');
    }

    //relación N a N
    public function roles(){
        return $this->belongsToMany('App\Role');
    }

    //relación N a N con pivot
    public function roles_pivot(){
        return $this->belongsToMany('App\Role')->withPivot('created_at');
    }

    //--------------------
    //version 2
    //-------------------
    //ralacion polimofica con photo
    public function photos(){
        return $this->morphMany('App\photo', 'imageable');
    }
}
