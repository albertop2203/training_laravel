<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePostRequest;
use App\Post;
use Illuminate\Http\Request;

class PostsController extends Controller
{ 
    public function index()
    {
        $posts = Post::all();
        return view('post.index', compact('posts'));
    } 
    public function create()
    { 
        return view('post.create');
    } 
    public function store(CreatePostRequest $request)
    { 
        $input = $request->all();
        if($file = $request->file('file')){
            $name = $file->getClientOriginalName();
            $file->move('images', $name);
            $input['path'] = $name;

        } 
        Post::create($input);
        return redirect('posts');
    } 
    public function show($id)
    {
        $post = Post::findOrFail($id);
        return view('post.show', compact('post'));
    } 
    public function edit($id)
    {
        $post = Post::findOrFail($id);
        return view('post.edit', compact('post'));
    } 
    public function update(CreatePostRequest $request, $id)
    { 
        Post::findOrFail($id)->update($request->all());
        return redirect('posts');
    } 
    public function destroy($id)
    {
        Post::findOrFail($id)->delete();
        return redirect('posts');
    }
}
