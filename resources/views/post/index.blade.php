<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Publicaciones</title>
</head>
<body> 
    <h1>Publicaciones</h1>  
    <a href="{{route('posts.create')}}">Nuevo</a>
    <table>
        <thead>
            <tr>
                <th>
                    Titulo
                </th>
                <th>
                    Contenido
                </th>
                <th>
                    Imagen
                </th>
                <th>
                    Opciones
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach ($posts as $post)  
                <tr>
                    <td>
                         {{$post->title}}
                    </td>
                    <td>
                        {{$post->content}}
                    </td>
                    <td>
                        <div class="container">
                            <img height="100" src="{{$post->path}}" alt=""> 
                        </div> 
                    </td>
                    <td>
                        <a href="{{route('posts.show', $post->id)}}"> Detalles</a>
                        <a href="{{route('posts.edit', $post->id)}}"> Editar</a>  
                        {!! Form::open(['method' => 'DELETE', 'action' => ['PostsController@destroy', $post->id]]) !!} 
                        {{ csrf_field() }}   
                        {!! Form::submit('Eliminar', ['class' => 'form-control']) !!}
                    {!! Form::close() !!} 
                    </td>
                </tr> 
            @endforeach
        </tbody>
    </table>
    
</body>
</html>