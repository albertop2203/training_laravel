<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Post</title>
</head>
<body>
    <h1>Detalles</h1>
    <ul>
        <li>{{$post->title}}</li>
        <li>{{$post->content}}</li>
    </ul>
    <a href="{{route('posts.edit', $post->id)}}">Editar</a>
</body>
</html>