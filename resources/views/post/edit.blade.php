<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Publicación</title>
</head>
<body> 
    {!! Form::model($post, ['method' => 'PATCH', 'action' => ['PostsController@update', $post->id]]) !!} 
        {{ csrf_field() }} 
        <div class="form-group">
            {!! Form::label('Titulo', 'Titulo') !!}
            {!! Form::text('title', null, ['class' => 'form-control']) !!}
        </div>  
        <div class="form-group">
            {!! Form::label('Contenido', 'Contenido') !!}
            {!! Form::text('content', null, ['class' => 'form-control']) !!}
        </div>   
        {!! Form::submit('Actualizar', ['class' => 'form-control']) !!}
    {!! Form::close() !!} 
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>
                    {{$error}}
                </li>
            @endforeach    
        </div> 
    @endif
</body>
</html>