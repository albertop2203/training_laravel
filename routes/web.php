<?php

use Illuminate\Support\Facades\Route;
/* use App\Post;
use App\User;
use App\Country;

use App\Photo;
use App\Tag;
use App\Video; */

Route::get('/', function () {
    return view('welcome');
});

Route::resource('/posts', 'PostsController');

//he comentado las siguientes rutas ya que son del primer proyecto y estas mismas no se usarán.
/* 
    //Ruta para la inserción de datos quemados a la db
    Route::get('/post/insert', function () {
        $result = DB::insert('INSERT INTO posts(title, content) 
                            VALUES(?,?)', 
                            array('Training laravel', 'This is the training'));
        return $result;
    });
    //Ruta para la edición de datos quemados a la db
    Route::get('/post/update', function () {
        $result = DB::update('UPDATE posts 
                            SET title = "Update title" 
                            WHERE id = ?', 
                            array(2));
        return $result;
    }); 
    //Ruta para el listado de registros
    Route::get('/post', function () {
        $result = DB::select('SELECT title, content
                            FROM posts');
        return count($result) ? $result : 'Without data';
    });
    //Ruta para la eliminación de registros
    Route::get('/post/delete', function () {
        $result = DB::delete('DELETE FROM posts 
                            WHERE id=?', 
                            array(1));
        return $result;
    });
    // ----------------------------------------------
    //Rutas para consultas eloquent
    // ----------------------------------------------
    //Mostrar todos los registros de la db
    Route::get('/eloquent', function() {
        $eloquentRes = Post::all(); 
        foreach($eloquentRes as $post){
            echo 'Id: ' . $post->id .', Title: ' . $post->title . ', Content: '. $post->content . '<br>';
        };
    });
    //Insertar nuevo registro
    Route::get('/eloquent/insert', function() {
        $post = new Post; 
        $post->title    = "Eloquent insert";
        $post->content  = "This is new content";
        $post->save();
    });
    //Insertar nuevo registro
    Route::get('/eloquent/create', function() { 
        Post::create(array('title' => 'eloquent create', 'content' => 'Content created'));
    });
    //Actualizar registro
    Route::get('/eloquent/update', function() {
        $post =  Post::find(2); 
        $post->title    = "Eloquent update";
        $post->content  = "Content updated";
        $post->save();
    });
    //Eliminar registro
    Route::get('/eloquent/delete', function() {
        Post::find(2)->delete();   
    });
    //Destruir registro
    Route::get('/eloquent/destroy', function() {
        //se puede destruir 1 o varios
        //Post::destroy(3);   
        Post::destroy(array(4, 5));   
    });
    //Eliminar registro con softDelete
    Route::get('/eloquent/softDelete/delete', function() {
        Post::find(6)->delete();   
    });
    //Buscar registros por medio de id que fueron eliminados por softdelete
    Route::get('/eloquent/softDelete/find', function() {
        return Post::withTrashed()->where('id', 6)->get();   
    }); 
    Route::get('/eloquent/softDelete', function() {
        return Post::onlyTrashed(6)->get();   
    });
    //restaurar registros eliminados por medio de softdelete
    Route::get('/eloquent/softDelete/restore', function() {
        return Post::withTrashed()->where('id', 6)->restore();   
    });
    //Forzar eliminación
    Route::get('/eloquent/softDelete/forceDelete', function() {
        return Post::withTrashed()->where('id', 7)->forceDelete();   
    });

    //  ----------------------------
    // Mostrando relaciones
    // -----------------------------

    // Mostrar el post del user
    Route::get('/relation/user/{id}/post', function($id){
        return User::find($id)->post;
    });
    // Mostrar el user del post
    Route::get('/relation/post/{id}/user', function($id){
        return Post::find($id)->user;
    });

    // Mostrar los post de un user
    Route::get('/relation/user/{id}/posts', function($id){
        $user = User::find($id);
        foreach ($user->posts as $post) {
            echo $post . '<br>';
        }
    }); 

    // Mostrar roles de un usuario
    Route::get('/relation/user/{id}/roles', function($id){
        $user = User::find($id);
        foreach ($user->roles as $role) {
            echo $role ;
        }

        //también podemos usarel siguiente porcedimiento más detallado
        // $user = User::find($id)->roles()->orderBy('id', 'desc')->get();
        // return $user;
    });

    // Mostrar datos de la table intermedia entre rol y usuario
    Route::get('/relation/pivot/user/{id}/roles', function($id){
        $user = User::find($id);
        foreach ($user->roles_pivot as $role) {
            echo $role->pivot->created_at . '<br>';
        } 
    });

    //Mostrar pais de una publicación
    Route::get('relation/user/country', function(){
        $country = Country::find(1);
        foreach ($country->posts as $post) {
            echo $post->title . '<br>';
        }
    });


    // -----------------------
    // version 2 
    // ------------------------

    //Relación polimorfica user photo
    Route::get('relation/polymorph/user', function(){
        $user = User::find(1);

        foreach ($user->photos as $photo) {
        echo $photo . '<br>';
        }
    });

    //Relación polimorfica post photo
    Route::get('relation/polymorph/post', function(){
        $post = Post::find(1);

        foreach ($post->photos as $photo) {
        echo $photo . '<br>';
        }
    });

    //relacion polimorfica photo (user, post)
    Route::get('relation/polymorph/photo/{id}', function($id){
        $photo = Photo::findOrFail($id); 
        return $photo->imageable;
    });

    //relacion polimorfica muchos a muchos post tag
    Route::get('relation/polymorph/post/tag', function(){
        $post = Post::find(1); 
        foreach ($post->tags as $tag) {
            echo $tag . '<br>';
        }
    });

    //relacion polimorfica muchos a muchos tag post
    Route::get('relation/polymorph/tag/post', function(){
        $tag = Tag::find(2); 
        foreach ($tag->posts as $post) {
            echo $post . '<br>';
        }
    });

    //relacion polimorfica muchos a muchos post tag
    Route::get('relation/polymorph/video/tag', function(){
        $video = Video::find(1); 
        foreach ($video->tags as $tag) {
            echo $tag . '<br>';
        }
    });

    //relacion polimorfica muchos a muchos tag video
    Route::get('relation/polymorph/tag/video', function(){
        $tag = Tag::find(1); 
        foreach ($tag->video as $post) {
            echo $post . '<br>';
        }
    });
    
    // -----------------------
    // 
    // ------------------------ 
*/