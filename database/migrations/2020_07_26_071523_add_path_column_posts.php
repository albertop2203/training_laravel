<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPathColumnPosts extends Migration
{
    //agregar columna path para almacenar la ruta de la imagen en post
    public function up()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->string('path');
        });
    }
    public function down()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->dropColumn('path');
        });
    }
}
